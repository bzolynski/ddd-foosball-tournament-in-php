<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Shared;

abstract class Entity
{
    protected $id;

    public function id(): int
    {
        return $this->id;
    }
}
