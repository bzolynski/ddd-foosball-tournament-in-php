<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\Policy;

use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInit;

class StartTournamentPolicy
{
    public static function isAllowed(TournamentInit $tournament)
    {
        return count($tournament->teams()->getAll()) >= 2;
    }
}
