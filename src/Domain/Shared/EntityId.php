<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Shared;

abstract class EntityId
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getValue(): int
    {
        return $this->id;
    }

    public function equals(EntityId $id): bool
    {
        return $this->id === $id->getValue();
    }
}
