<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\Model;

use CODEfactors\Foosball\Domain\Match\Model\Match;
use CODEfactors\Foosball\Domain\Match\Model\MatchFinished;

class MatchesCollection
{
    private $collection;

    public function __construct()
    {
        $this->collection = array();
    }

    public function add(Match $match)
    {
        $this->collection[] = $match;
    }

    public function setResult(MatchFinished $matchFinished)
    {
        foreach ($this->collection as $key => $aMatch) {
            if ($matchFinished->id() === $aMatch->id()) {
                $this->collection[$key] = $matchFinished;
            }
        }
    }

    public function getAll(): array
    {
        return $this->collection;
    }

    public function isEmpty(): bool
    {
        return empty($this->collection);
    }
}
