<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Match\ValueObject;

use CODEfactors\Foosball\Domain\Shared\EntityId;

class MatchId extends EntityId
{

}
