<?php

declare(strict_types=1);

use CODEfactors\Foosball\UserInterface\TournamentApi;

// Init autoload
require_once '../vendor/autoload.php';

// Instantiate app
$app = new TournamentApi();

// Define app routes
require_once '../config/routes.php';

// Run app
$app->run();
