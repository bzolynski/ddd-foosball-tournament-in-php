<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Match\Model;

use CODEfactors\Foosball\Domain\Match\Exception\InvalidMatchArgumentException;
use CODEfactors\Foosball\Domain\Match\ValueObject\MatchId;
use CODEfactors\Foosball\Domain\Shared\Entity;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;
use CODEfactors\Foosball\Domain\Tournament\ValueObject\TournamentId;

class Match extends Entity implements MatchInterface
{
    private $firstTeamId;

    private $secondTeamId;

    private $tournamentId;

    public function __construct(MatchId $matchId, TournamentId $tournamentId, TeamId $firstTeamId, TeamId $secondTeamId)
    {
        if ($firstTeamId->getValue() === $secondTeamId->getValue()) {
            throw new InvalidMatchArgumentException(
                'Team with id: ' . $firstTeamId->getValue() . ' cannot play against itself'
            );
        }
        $this->tournamentId = $tournamentId->getValue();
        $this->id = $matchId->getValue();
        $this->firstTeamId = $firstTeamId;
        $this->secondTeamId = $secondTeamId;
    }

    public function tournamentId(): int
    {
        return $this->tournamentId;
    }

    public function firstTeamId(): TeamId
    {
        return $this->firstTeamId;
    }

    public function secondTeamId(): TeamId
    {
        return $this->secondTeamId;
    }

    public function finish(int $firstTeamScore, int $secondTeamScore): MatchFinished {
        return new MatchFinished($this, $firstTeamScore, $secondTeamScore);
    }

    public function equals(Match $match): bool {
        $orderedMatching = $match->firstTeamId()->equals($this->firstTeamId())
            && $match->secondTeamId()->equals($this->secondTeamId());

        $swappedMatching = $match->firstTeamId()->equals($this->secondTeamId())
            && $match->secondTeamId()->equals($this->firstTeamId());

        return $match->tournamentId() === $this->tournamentId() && ($orderedMatching || $swappedMatching);
    }
}
