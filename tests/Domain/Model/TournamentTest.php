<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Tests\Domain\Model;

use CODEfactors\Foosball\Domain\Match\Model\Match;
use CODEfactors\Foosball\Domain\Match\Service\MatchesInTournamentService;
use CODEfactors\Foosball\Domain\Match\ValueObject\MatchId;
use CODEfactors\Foosball\Domain\Shared\IdentityGenerator;
use CODEfactors\Foosball\Domain\Team\Exception\DuplicatedTeamException;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;
use CODEfactors\Foosball\Domain\Tournament\Exception\TournamentStartException;
use CODEfactors\Foosball\Domain\Team\Factory\TeamFactory;
use CODEfactors\Foosball\Domain\Tournament\Model\MatchesCollection;
use CODEfactors\Foosball\Domain\Tournament\Model\TeamsIdCollection;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInit;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInProgress;
use CODEfactors\Foosball\Domain\Tournament\ValueObject\TournamentId;
use PHPUnit\Framework\TestCase;

class TournamentTest extends TestCase
{
    public function testInstantiation()
    {
        $id = IdentityGenerator::get();
        $tournament = new TournamentInit(new TournamentId($id));
        foreach ($this->getTeams() as $team) {
            $tournament->addTeam($team);
        }
        $this->assertInstanceOf(TournamentInit::class, $tournament);

        $tournament = $tournament->start(new MatchesCollection());
        $this->assertInstanceOf(TournamentInProgress::class, $tournament);
    }

    public function testInvalidTeamsInstantiation()
    {
        $this->expectException(DuplicatedTeamException::class);
        $id = IdentityGenerator::get();
        $tournament = new TournamentInit(new TournamentId($id));
        foreach ($this->getInvalidTeams() as $team) {
            $tournament->addTeam($team);
        }
    }

    public function testFailWhenStartingTournamentWithOnlyOneTeam()
    {
        $this->expectException(TournamentStartException::class);
        $id = IdentityGenerator::get();
        $tournament = new TournamentInit(new TournamentId($id));
        $teams = $this->getTeams();
        $tournament->addTeam($teams[0]);
        $tournament->start();
    }

    /**
     * @dataProvider resultScenarios
     * @param int $scenarioIndex
     * @param int $winnerTeamId
     */
    public function testWinningTeam(int $scenarioIndex, int $winnerTeamId)
    {
        $results = $this->getResults($scenarioIndex);

        $tournamentId = new TournamentId(1);
        $tournament = new TournamentInit($tournamentId);
        $teams = $this->getTeams();
        foreach ($teams as $team) {
            $tournament->addTeam($team);
        }

        $teamsIdCollection = new TeamsIdCollection();
        foreach ($this->getTeams() as $team) {
            $teamsIdCollection->add($team);
        }
        $matchesCollection = new MatchesCollection();
        $matchesService = new MatchesInTournamentService($teamsIdCollection, $tournamentId);
        $matches = $matchesService->getMatchesCollection();
        $tournamentInProgress = $tournament->start();

        /** @var Match $match */
        foreach ($matches->getAll() as $matchOrder => $match) {
            $matchesCollection->add($match);
            $tournamentInProgress->setMatchResult(
                $match,
                $results[$matchOrder][0],
                $results[$matchOrder][1]
            );

            $this->echoResults(false, $match, $results, $matchOrder);
        }
        $this->assertCount(6, $tournamentInProgress->matches()->getAll());

        $tournamentFinished = $tournamentInProgress->finish();
        $this->assertSame($winnerTeamId, $tournamentFinished->getWinnerTeamId());
    }

    private function getTeams()
    {
        $teamFactory = new TeamFactory();
        return array(
            $teamFactory->create(
                new TeamId(1), 'Team #1', 'Pele', 'Edwin van der Sar'
            ),
            $teamFactory->create(
                new TeamId(2), 'Team #2', 'Alessandro Del Piero', 'Gianluigi Buffon'
            ),
            $teamFactory->create(
                new TeamId(3), 'Team #3', 'Diego Maradona', 'David Seaman'
            ),
            $teamFactory->create(
                new TeamId(4), 'Team #4', 'Zinedine Zidane', 'Peter Schmeichel'
            )
        );
    }

    private function getInvalidTeams()
    {
        $teamFactory = new TeamFactory();
        return array(
            $teamFactory->create(
                new TeamId(1), 'Team #1', 'Pele', 'Edwin van der Sar'
            ),
            $teamFactory->create(
                new TeamId(1), 'Team #1', 'Alessandro Del Piero', 'Gianluigi Buffon'
            )
        );
    }

    public function resultScenarios(): array
    {
        return array(
            array(
                0, 4
            ),
            array(
                1, 1
            )
        );
    }

    private function getResults(int $index): array
    {
        $results = array(
            array(
                /* Team #1 and Team #4 have the same amount of points (5 points),
                 * but Team #4 has 7 goals, and Team #1 has 5 goals
                 * [Team #1 : Team #2] 4 : 3
                 * [Team #1 : Team #3] 0 : 0
                 * [Team #1 : Team #4] 1 : 1
                 * [Team #2 : Team #3] 5 : 5
                 * [Team #2 : Team #4] 2 : 3
                 * [Team #3 : Team #4] 3 : 3
                 */
                array(4, 3),
                array(0, 0),
                array(1, 1),
                array(5, 5),
                array(2, 3),
                array(3, 3)
            ),
            array(
                /* Team #1 and Team #4 have the same amount of points (5 points),
                 * and both teams have 7 goals, but Team #1 has 4 goals against, and Team #4 has 6 goals against
                 * [Team #1 : Team #2] 6 : 3
                 * [Team #1 : Team #3] 0 : 0
                 * [Team #1 : Team #4] 1 : 1
                 * [Team #2 : Team #3] 5 : 5
                 * [Team #2 : Team #4] 2 : 3
                 * [Team #3 : Team #4] 3 : 3
                 */
                array(6, 3),
                array(0, 0),
                array(1, 1),
                array(5, 5),
                array(2, 3),
                array(3, 3)
            )
        );
        return $results[$index];
    }

    private function echoResults($display = false, Match $match, array $results, int $matchOrder): void
    {
        if (!$display) {
            return;
        }
        if ($matchOrder === 0) {
            echo PHP_EOL . PHP_EOL;
        }
        echo '[Team #' . $match->firstTeamId()->getValue() .
            ' : Team #' . $match->secondTeamId()->getValue() . '] ' .
            $results[$matchOrder][0] . ' : ' . $results[$matchOrder][1] . PHP_EOL;
    }
}
