<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\UserInterface\Response;

use CODEfactors\Foosball\Domain\Tournament\Model\Tournament;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInit;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInProgress;

class TournamentResponse
{
    public $id;

    public $state;

    public function __construct(Tournament $tournament)
    {
        $this->id = $tournament->id();
        $this->state = $this->getState($tournament);
    }

    public function getState(Tournament $tournament): string
    {
        if ($tournament instanceof TournamentInit) {
            return 'init';
        } else if ($tournament instanceof TournamentInProgress) {
            return 'progress';
        } else {
            return 'finished';
        }
    }
}
