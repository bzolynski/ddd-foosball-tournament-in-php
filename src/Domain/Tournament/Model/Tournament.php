<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\Model;

use CODEfactors\Foosball\Domain\Shared\AggregateRoot;

abstract class Tournament extends AggregateRoot
{
    /**
     * @var TeamsIdCollection
     */
    protected $teams;

    /**
     * @var MatchesCollection
     */
    protected $matches;

    public function teams(): TeamsIdCollection
    {
        return $this->teams;
    }

    public function matches(): MatchesCollection
    {
        return $this->matches;
    }
}
