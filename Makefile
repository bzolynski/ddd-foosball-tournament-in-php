all : composer
.PHONY: all start stop composer phpunit db-drop db-create db-fixtures build bash

start :
	docker-compose up -d

stop :
	docker-compose down

build :
	cd docker && ./build.sh && cd ./..

logs :
	docker logs -f dddfoosballtournamentinphp_php_1

composer :
	docker-compose run --rm --no-deps php composer install

composer-require :
	docker-compose run --rm --no-deps php composer require $(package)

composer-require-dev :
	docker-compose run --rm --no-deps php composer require --dev $(package)

composer-remove-dev :
	docker-compose run --rm --no-deps php composer remove --dev $(package)

composer-dump-autoload :
	docker-compose run --rm --no-deps app composer dump-autoload

phpunit :
	docker-compose run --rm --no-deps php vendor/bin/phpunit tests

phpunit-coverage-text :
	docker-compose run --rm --no-deps php vendor/bin/phpunit tests --coverage-text

phpunit-coverage-html :
	docker-compose run --rm --no-deps php vendor/bin/phpunit tests --coverage-html coverage/

db-drop :
	docker-compose run --rm --no-deps php php doctrine-cli.php orm:schema-tool:drop --force --em=dev

db-create :
	docker-compose run --rm --no-deps php php doctrine-cli.php orm:schema-tool:create --em=dev

db-update :
	docker-compose run --rm --no-deps php php doctrine-cli.php orm:schema-tool:update --force --em=dev

db-fixtures :
	docker-compose run --rm --no-deps php php doctrine-cli.php doctrine:fixtures:load --em-dev

bash :
	docker exec -i -t dddfoosballtournamentinphp_php_1 env TERM=xterm bash
