<?php

declare(strict_types=1);

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use CODEfactors\Foosball\Infrastructure\Persistence\Doctrine\EntityManagerServiceFactory;

chdir('src');

require_once '../vendor/autoload.php';

$environmentName = '';
foreach ($_SERVER['argv'] as $index => $arg) {
    $e = explode('=', $arg);
    $key = str_replace('-', '', $e[0]);
    if ($key === 'em') {
        $environmentName = $e[1];
        unset($_SERVER['argv'][$index]);
    }
}

$entityManagerServiceFactory = new EntityManagerServiceFactory();
$service = $entityManagerServiceFactory::create($environmentName);

return ConsoleRunner::createHelperSet($service->getEntityManager());
