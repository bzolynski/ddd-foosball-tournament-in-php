<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\Repository;

use CODEfactors\Foosball\Domain\Match\Model\Match;
use CODEfactors\Foosball\Domain\Match\Model\MatchFinished;
use CODEfactors\Foosball\Domain\Team\Model\Team;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentFinished;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInit;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInProgress;
use CODEfactors\Foosball\Domain\Tournament\ValueObject\TournamentId;

interface TournamentRepositoryInterface
{
    public function create(): TournamentInit;

    public function start(TournamentInit $tournament): TournamentInProgress;

    public function finish(TournamentInProgress $tournament): TournamentFinished;

    public function addTeam(TournamentInit $tournament, Team $team): Team;

    public function getTournamentInInitStateById(TournamentId $tournamentId): TournamentInit;

    public function getTournamentInProgressStateById(TournamentId $tournamentId): TournamentInProgress;

    public function getTournamentInFinishedStateById(TournamentId $tournamentId): TournamentFinished;

    public function findTournamentsInInitState(): array;

    public function findTournamentsInProgressState(): array;

    public function findTournamentsInFinishedState(): array;

    public function finishMatch(
        TournamentInProgress $tournament,
        Match $match,
        int $firstTeamScore,
        int $secondTeamScore): MatchFinished;
}
