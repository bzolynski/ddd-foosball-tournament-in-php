<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Infrastructure\Persistence\InMemory;

use CODEfactors\Foosball\Domain\Match\Model\Match;
use CODEfactors\Foosball\Domain\Match\Model\MatchFinished;
use CODEfactors\Foosball\Domain\Shared\IdentityGenerator;
use CODEfactors\Foosball\Domain\Team\Model\Team;
use CODEfactors\Foosball\Domain\Tournament\Model\Tournament;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentFinished;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInit;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInProgress;
use CODEfactors\Foosball\Domain\Tournament\Repository\TournamentRepositoryInterface;
use CODEfactors\Foosball\Domain\Tournament\ValueObject\TournamentId;
use CODEfactors\Foosball\Infrastructure\Exception\TournamentNotFoundException;

class InMemoryTournamentRepository extends InMemoryRepository implements TournamentRepositoryInterface
{
    public function create(): TournamentInit
    {
        $id = IdentityGenerator::get();
        $tournament = new TournamentInit(new TournamentId($id));
        return $this->persist($tournament);
    }

    public function start(TournamentInit $tournamentInit): TournamentInProgress
    {
        $tournamentInProgress = $tournamentInit->start();
        $this->persist($tournamentInProgress);
        return $this->deepClone($tournamentInProgress);
    }

    public function finish(TournamentInProgress $tournamentInProgress): TournamentFinished
    {
        $tournamentFinished = $tournamentInProgress->finish();
        $this->persist($tournamentFinished);
        return $this->deepClone($tournamentFinished);
    }

    public function addTeam(TournamentInit $tournament, Team $team): Team
    {
        $tournament->addTeam($team);
        $this->persist($tournament);
        return $this->deepClone($team);
    }

    public function getTournamentInInitStateById(TournamentId $tournamentId): TournamentInit
    {
        if (empty($this->records)) {
            return $this->throwTournamentNotFoundException($tournamentId, 'init');
        }
        /**
         * @var Tournament $tournament
         */
        foreach ($this->records as $tournament) {
            if ($tournamentId->equals(new TournamentId($tournament->id()))) {
                if ($tournament instanceof TournamentInit) {
                    return $this->deepClone($tournament);
                }
                return $this->throwTournamentNotFoundException($tournamentId, 'init');
            }
        }
        return $this->throwTournamentNotFoundException($tournamentId, 'init');
    }

    public function getTournamentInProgressStateById(TournamentId $tournamentId): TournamentInProgress
    {
        if (empty($this->records)) {
            return $this->throwTournamentNotFoundException($tournamentId, 'progress');
        }
        /**
         * @var Tournament $tournament
         */
        foreach ($this->records as $tournament) {
            if ($tournamentId->equals(new TournamentId($tournament->id()))) {
                if ($tournament instanceof TournamentInProgress) {
                    return $this->deepClone($tournament);
                }
                return $this->throwTournamentNotFoundException($tournamentId, 'progress');
            }
        }
        return $this->throwTournamentNotFoundException($tournamentId, 'progress');
    }

    public function getTournamentInFinishedStateById(TournamentId $tournamentId): TournamentFinished
    {
        if (empty($this->records)) {
            return $this->throwTournamentNotFoundException($tournamentId, 'finished');
        }
        foreach ($this->records as $tournament) {
            /** @var Tournament $tournament */
            if ($tournamentId->equals(new TournamentId($tournament->id()))) {
                if ($tournament instanceof TournamentFinished) {
                    return $this->deepClone($tournament);
                }
                return $this->throwTournamentNotFoundException($tournamentId, 'finished');
            }
        }
        return $this->throwTournamentNotFoundException($tournamentId, 'finished');
    }

    public function findTournamentsInInitState(): array
    {
        if (empty($this->records)) {
            return [];
        }
        $tournamentsList = [];
        foreach ($this->records as $tournament) {
            if ($tournament instanceof TournamentInit) {
                $tournamentsList[] = $this->deepClone($tournament);
            }
        }
        return $tournamentsList;
    }

    public function findTournamentsInProgressState(): array
    {
        if (empty($this->records)) {
            return [];
        }
        $tournamentsList = [];
        foreach ($this->records as $tournament) {
            if ($tournament instanceof TournamentInProgress) {
                $tournamentsList[] = $this->deepClone($tournament);
            }
        }
        return $tournamentsList;
    }

    public function findTournamentsInFinishedState(): array
    {
        if (empty($this->records)) {
            return [];
        }
        $tournamentsList = [];
        foreach ($this->records as $tournament) {
            if ($tournament instanceof TournamentFinished) {
                $tournamentsList[] = $this->deepClone($tournament);
            }
        }
        return $tournamentsList;
    }

    public function finishMatch(
        TournamentInProgress $tournament,
        Match $match,
        int $firstTeamScore,
        int $secondTeamScore): MatchFinished
    {
        $matchFinished = $tournament->setMatchResult($match, $firstTeamScore, $secondTeamScore);
        $this->persist($tournament);
        return $this->deepClone($matchFinished);
    }

    private function throwTournamentNotFoundException(TournamentId $tournamentId, string $state)
    {
        throw new TournamentNotFoundException(
            'Tournament in state: ' . $state . ' with id: ' . $tournamentId->getValue() . ' not found'
        );
    }
}
