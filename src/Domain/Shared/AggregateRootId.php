<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Shared;

abstract class AggregateRootId
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getValue(): int
    {
        return $this->id;
    }

    public function equals(AggregateRootId $id): bool
    {
        return $this->id === $id->getValue();
    }
}
