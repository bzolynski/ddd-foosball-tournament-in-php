<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\Policy;

use CODEfactors\Foosball\Domain\Match\Model\MatchFinished;
use CODEfactors\Foosball\Domain\Tournament\Model\MatchesCollection;

class FinishTournamentPolicy
{
    public static function isAllowed(MatchesCollection $matchesCollection)
    {
        foreach ($matchesCollection->getAll() as $match) {
            if (!$match instanceof MatchFinished) {
                return false;
            }
        }
        return true;
    }
}
