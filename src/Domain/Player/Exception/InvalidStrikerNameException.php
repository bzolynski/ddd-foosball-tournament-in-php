<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Player\Exception;

use Exception;

class InvalidStrikerNameException extends Exception
{

}
