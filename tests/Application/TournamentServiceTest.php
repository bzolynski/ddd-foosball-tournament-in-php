<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Tests\Application;

use CODEfactors\Foosball\Application\TournamentService;
use CODEfactors\Foosball\Domain\Match\Exception\MatchNotFoundException;
use CODEfactors\Foosball\Domain\Match\Model\Match;
use CODEfactors\Foosball\Domain\Match\Model\MatchFinished;
use CODEfactors\Foosball\Domain\Team\Exception\TeamNotFoundException;
use CODEfactors\Foosball\Domain\Team\Model\Team;
use CODEfactors\Foosball\Domain\Tournament\Exception\TournamentFinishException;
use CODEfactors\Foosball\Domain\Tournament\Model\Tournament;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentFinished;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInit;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInProgress;
use CODEfactors\Foosball\Infrastructure\Exception\TournamentNotFoundException;
use CODEfactors\Foosball\Infrastructure\Persistence\InMemory\InMemoryTeamRepository;
use CODEfactors\Foosball\Infrastructure\Persistence\InMemory\InMemoryTournamentRepository;
use PHPUnit\Framework\TestCase;

class TournamentServiceTest extends TestCase
{
    /**
     * @var TournamentService
     */
    private $tournamentService;

    public function setUp()
    {
        $tournamentRepository = new InMemoryTournamentRepository();
        $teamRepository = new InMemoryTeamRepository();
        $this->tournamentService = new TournamentService(
            $tournamentRepository,
            $teamRepository
        );
    }

    public function testTournamentInInitStateDoesNotExistOnEmptyList()
    {
        $this->expectException(TournamentNotFoundException::class);
        $this->tournamentService->getTournamentInInitState(1);
    }

    public function testTournamentInProgressStateDoesNotExistOnEmptyList()
    {
        $this->expectException(TournamentNotFoundException::class);
        $this->tournamentService->getTournamentInProgressState(1);
    }

    public function testTournamentInFinishedStateDoesNotExistOnEmptyList()
    {
        $this->expectException(TournamentNotFoundException::class);
        $this->tournamentService->getTournamentInFinishedState(1);
    }

    public function testTournamentNotInProgressState()
    {
        $tournament = $this->tournamentService->createTournament();
        $this->expectException(TournamentNotFoundException::class);
        $this->tournamentService->getTournamentInProgressState($tournament->id());
    }

    public function testTournamentNotInFinishedState()
    {
        $tournament = $this->tournamentService->createTournament();
        $this->expectException(TournamentNotFoundException::class);
        $this->tournamentService->getTournamentInFinishedState($tournament->id());
    }

    public function testTournamentInInitStateDoesNotExist()
    {
        $this->tournamentService->createTournament();
        $this->expectException(TournamentNotFoundException::class);
        $this->tournamentService->getTournamentInInitState(12345);
    }

    public function testTournamentInProgressStateDoesNotExist()
    {
        $this->tournamentService->createTournament();
        $this->expectException(TournamentNotFoundException::class);
        $this->tournamentService->getTournamentInProgressState(12345);
    }

    public function testTournamentInFinishedStateDoesNotExist()
    {
        $this->tournamentService->createTournament();
        $this->expectException(TournamentNotFoundException::class);
        $this->tournamentService->getTournamentInFinishedState(12345);
    }

    public function testTournamentStarted()
    {
        $tournament = $this->tournamentService->createTournament();
        $tournamentId = $tournament->id();
        $tournament = $this->tournamentService->getTournamentInInitState($tournamentId);
        $this->assertInstanceOf(Tournament::class, $tournament);
    }

    public function testPlayTournament()
    {
        $this->assertCount(0, $this->tournamentService->findTournamentsInInitState());
        $this->assertCount(0, $this->tournamentService->findTournamentsInProgressState());
        $this->assertCount(0, $this->tournamentService->findTournamentsInFinishedState());

        $tournament = $this->tournamentService->createTournament();
        $tournamentId = $tournament->id();

        $team1 = $this->tournamentService->createTeam(
            'Team #1', 'Pele', 'Edwin van der Sar'
        );
        $this->assertInstanceOf(Team::class, $team1);

        $team2 = $this->tournamentService->createTeam(
            'Team #2', 'Alessandro Del Piero', 'Gianluigi Buffon'
        );
        $this->assertInstanceOf(Team::class, $team2);

        $team3 = $this->tournamentService->createTeam(
            'Team #3', 'Diego Maradona', 'David Seaman'
        );
        $this->assertInstanceOf(Team::class, $team3);

        $team4 = $this->tournamentService->createTeam(
            'Team #4', 'Zinedine Zidane', 'Peter Schmeichel'
        );
        $this->assertInstanceOf(Team::class, $team4);

        $this->tournamentService->addTeamToTournament($tournamentId, $team1->id());
        $this->tournamentService->addTeamToTournament($tournamentId, $team2->id());
        $this->tournamentService->addTeamToTournament($tournamentId, $team3->id());
        $this->tournamentService->addTeamToTournament($tournamentId, $team4->id());

        $this->assertCount(1, $this->tournamentService->findTournamentsInInitState());
        $this->assertCount(0, $this->tournamentService->findTournamentsInProgressState());
        $this->assertCount(0, $this->tournamentService->findTournamentsInFinishedState());

        $tournament = $this->tournamentService->getTournamentInInitState($tournamentId);
        $this->assertInstanceof(TournamentInit::class, $tournament);

        $this->tournamentService->startTournament($tournamentId);

        $this->assertCount(0, $this->tournamentService->findTournamentsInInitState());
        $this->assertCount(1, $this->tournamentService->findTournamentsInProgressState());
        $this->assertCount(0, $this->tournamentService->findTournamentsInFinishedState());

        $tournament = $this->tournamentService->getTournamentInProgressState($tournamentId);
        $this->assertInstanceof(TournamentInProgress::class, $tournament);

        $matches = $this->tournamentService->getMatchesForTournamentInProgress($tournamentId);
        $this->assertCount(6, $matches->getAll());

        $resultsTable = array(
            array('firstTeam' => 2, 'secondTeam' => 5),
            array('firstTeam' => 3, 'secondTeam' => 2),
            array('firstTeam' => 3, 'secondTeam' => 3),
            array('firstTeam' => 2, 'secondTeam' => 1),
            array('firstTeam' => 0, 'secondTeam' => 0),
            array('firstTeam' => 0, 'secondTeam' => 2)
        );

        $results = array();
        /** @var Match $match */
        foreach ($matches->getAll() as $key => $match) {
            $results[$match->id()] = array(
                'firstTeam' => $resultsTable[$key]['firstTeam'],
                'secondTeam' => $resultsTable[$key]['secondTeam']
            );
        }

        /** @var Match $match */
        foreach ($matches->getAll() as $key => $match) {
            $this->tournamentService->finishMatch(
                $tournamentId,
                $match->id(),
                $results[$match->id()]['firstTeam'],
                $results[$match->id()]['secondTeam']
            );
        }
        $matches = $this->tournamentService->getMatchesForTournamentInProgress($tournamentId);
        $this->assertCount(6, $matches->getAll());

        /** @var MatchFinished $matchFinished */
        foreach ($matches as $matchFinished) {
            $this->assertInstanceOf(MatchFinished::class, $matchFinished);
            $this->assertSame($results[$matchFinished->id()]['firstTeam'], $matchFinished->firstTeamScore());
            $this->assertSame($results[$matchFinished->id()]['secondTeam'], $matchFinished->secondTeamScore());
        }

        $this->tournamentService->finishTournament($tournamentId);

        $this->assertCount(0, $this->tournamentService->findTournamentsInInitState());
        $this->assertCount(0, $this->tournamentService->findTournamentsInProgressState());
        $this->assertCount(1, $this->tournamentService->findTournamentsInFinishedState());

        $tournamentFinished = $this->tournamentService->getTournamentInFinishedState($tournamentId);
        $this->assertInstanceOf(TournamentFinished::class, $tournamentFinished);

        $this->assertSame($team2->id(), $tournamentFinished->getWinnerTeamId());
        $this->assertSame($team2->id(), $this->tournamentService->getWinnerTeamForTournament($tournamentId));
    }

    public function testRetrievingNonExistingTeam()
    {
        $this->expectException(TeamNotFoundException::class);
        $tournament = $this->tournamentService->createTournament();
        $tournamentId = $tournament->id();
        $this->tournamentService->addTeamToTournament($tournamentId, 1);
    }

    public function testSettingResultForAlreadyFinishedMatch()
    {
        $this->expectException(MatchNotFoundException::class);
        $tournament = $this->tournamentService->createTournament();
        $tournamentId = $tournament->id();
        $team1 = $this->tournamentService->createTeam(
            'Team #1', 'Pele', 'Edwin van der Sar'
        );
        $team2 = $this->tournamentService->createTeam(
            'Team #2', 'Alessandro Del Piero', 'Gianluigi Buffon'
        );
        $this->tournamentService->addTeamToTournament($tournamentId, $team1->id());
        $this->tournamentService->addTeamToTournament($tournamentId, $team2->id());
        $this->tournamentService->startTournament($tournamentId);
        $matches = $this->tournamentService->getMatchesForTournamentInProgress($tournamentId)->getAll();
        $this->assertCount(1, $matches);

        /** @var Match $match */
        $match = $matches[0];
        $matchId = $match->id();
        $this->tournamentService->finishMatch($tournamentId, $matchId, 1, 2);
        $this->tournamentService->finishMatch($tournamentId, $matchId, 1, 2);
    }

    public function testTryingToFinishTournamentWithSomeMatchesLeftToPlay()
    {
        $this->expectException(TournamentFinishException::class);
        $tournament = $this->tournamentService->createTournament();
        $tournamentId = $tournament->id();
        $team1 = $this->tournamentService->createTeam(
            'Team #1', 'Pele', 'Edwin van der Sar'
        );
        $team2 = $this->tournamentService->createTeam(
            'Team #2', 'Alessandro Del Piero', 'Gianluigi Buffon'
        );
        $team3 = $this->tournamentService->createTeam(
            'Team #3', 'Diego Maradona', 'David Seaman'
        );
        $this->tournamentService->addTeamToTournament($tournamentId, $team1->id());
        $this->tournamentService->addTeamToTournament($tournamentId, $team2->id());
        $this->tournamentService->addTeamToTournament($tournamentId, $team3->id());

        $this->tournamentService->startTournament($tournamentId);
        $matches = $this->tournamentService->getMatchesForTournamentInProgress($tournamentId)->getAll();
        $this->assertCount(3, $matches);

        /** @var Match $match */
        $match = $matches[0];
        $matchId = $match->id();
        $this->tournamentService->finishMatch($tournamentId, $matchId, 1, 2);

        $match = $matches[1];
        $matchId = $match->id();
        $this->tournamentService->finishMatch($tournamentId, $matchId, 0, 2);

        $this->tournamentService->finishTournament($tournamentId);
    }
}
