<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Team\Model;

use CODEfactors\Foosball\Domain\Player\Model\GoalKeeper;
use CODEfactors\Foosball\Domain\Player\Model\Striker;
use CODEfactors\Foosball\Domain\Shared\AggregateRoot;
use CODEfactors\Foosball\Domain\Team\Exception\InvalidTeamNameException;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;

class Team extends AggregateRoot
{
    private $name;

    private $striker;

    private $goalKeeper;

    public function __construct(TeamId $teamId, string $name, Striker $striker, GoalKeeper $goalKeeper)
    {
        if (empty(trim($name))) {
            throw new InvalidTeamNameException('Team name cannot be empty');
        }
        $this->id = $teamId->getValue();
        $this->name = $name;
        $this->striker = $striker;
        $this->goalKeeper = $goalKeeper;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function striker(): Striker
    {
        return $this->striker;
    }

    public function goalKeeper(): GoalKeeper
    {
        return $this->goalKeeper;
    }
}
