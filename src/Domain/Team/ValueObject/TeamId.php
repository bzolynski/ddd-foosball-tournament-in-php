<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Team\ValueObject;

use CODEfactors\Foosball\Domain\Shared\AggregateRootId;

class TeamId extends AggregateRootId
{

}
