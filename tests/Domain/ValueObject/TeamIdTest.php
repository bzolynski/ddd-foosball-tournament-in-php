<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Tests\Domain\ValueObject;

use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;
use PHPUnit\Framework\TestCase;

class TeamIdTest extends TestCase
{
    public function testEquals()
    {
        $teamId = new TeamId(1);
        $this->assertTrue($teamId->equals(new TeamId(1)));
        $this->assertSame(1, $teamId->getValue());
    }
}
