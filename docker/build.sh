#!/usr/bin/env bash
docker build -t ddd_foosball_in_php/php images/php
docker build -t ddd_foosball_in_php/nginx images/nginx
docker build -t ddd_foosball_in_php/mysql images/mysql
