<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\Model;

use CODEfactors\Foosball\Domain\Team\Model\Team;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;

class TeamsIdCollection
{
    private $collection;

    public function __construct()
    {
        $this->collection = array();
    }

    public function add(Team $team)
    {
        $this->collection[] = new TeamId($team->id());
    }

    public function getAll(): array
    {
        return $this->collection;
    }

    public function isEmpty(): bool
    {
        return empty($this->collection);
    }
}
