<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Player\Model;

use CODEfactors\Foosball\Domain\Player\Exception\InvalidGoalKeeperNameException;

class GoalKeeper
{
    private $name;

    public function __construct(string $name)
    {
        if (empty(trim($name))) {
            throw new InvalidGoalKeeperNameException('Goal keeper name cannot be empty');
        }
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }
}
