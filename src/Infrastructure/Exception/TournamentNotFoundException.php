<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Infrastructure\Exception;

use Exception;

class TournamentNotFoundException extends Exception
{

}