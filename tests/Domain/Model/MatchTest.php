<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Tests\Domain\Model;

use CODEfactors\Foosball\Domain\Match\Exception\InvalidMatchArgumentException;
use CODEfactors\Foosball\Domain\Match\Model\Match;
use CODEfactors\Foosball\Domain\Match\Model\MatchFinished;
use CODEfactors\Foosball\Domain\Match\ValueObject\MatchId;
use CODEfactors\Foosball\Domain\Team\Factory\TeamFactory;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;
use CODEfactors\Foosball\Domain\Tournament\ValueObject\TournamentId;
use PHPUnit\Framework\TestCase;

class MatchTest extends TestCase
{
    public function testInstantiation()
    {
        $teamFactory = new TeamFactory();
        $team1 = $teamFactory->create(
            new TeamId(1), 'Team #1', 'Pele', 'Edwin van der Sar'
        );
        $team2 = $teamFactory->create(
            new TeamId(2), 'Team #2', 'Alessandro Del Piero', 'Gianluigi Buffon'
        );
        $match = new Match(
            new MatchId(1),
            new TournamentId(1),
            new TeamId($team1->id()),
            new TeamId($team2->id())
        );

        $matchFinished = $match->finish(2, 3);
        $this->assertInstanceOf(MatchFinished::class, $matchFinished);
        $this->assertSame(2, $matchFinished->firstTeamScore());
        $this->assertSame(3, $matchFinished->secondTeamScore());
        $this->assertSame($team1->id(), $matchFinished->firstTeamId()->getValue());
        $this->assertSame($team2->id(), $matchFinished->secondTeamId()->getValue());
    }

    public function testMatchEquals()
    {
        $teamFactory = new TeamFactory();
        $team1 = $teamFactory->create(
            new TeamId(1), 'Team #1', 'Pele', 'Edwin van der Sar'
        );
        $team2 = $teamFactory->create(
            new TeamId(2), 'Team #2', 'Alessandro Del Piero', 'Gianluigi Buffon'
        );
        $team3 = $teamFactory->create(
            new TeamId(3), 'Team #3', 'Diego Maradona', 'David Seaman'
        );
        $match1 = new Match(
            new MatchId(1),
            new TournamentId(1),
            new TeamId($team1->id()),
            new TeamId($team2->id())
        );
        $match2 = new Match(
            new MatchId(2),
            new TournamentId(1),
            new TeamId($team2->id()),
            new TeamId($team1->id())
        );
        $match3 = new Match(
            new MatchId(2),
            new TournamentId(1),
            new TeamId($team2->id()),
            new TeamId($team3->id())
        );
        $match4 = new Match(
            new MatchId(2),
            new TournamentId(2),
            new TeamId($team1->id()),
            new TeamId($team2->id())
        );
        $match5 = new Match(
            new MatchId(2),
            new TournamentId(2),
            new TeamId($team2->id()),
            new TeamId($team1->id())
        );
        $this->assertTrue($match1->equals($match1));
        $this->assertTrue($match1->equals($match2));
        $this->assertFalse($match1->equals($match3));
        $this->assertFalse($match1->equals($match4));
        $this->assertFalse($match1->equals($match5));

        $match1Finished = $match1->finish(1, 1);
        $this->assertSame(1, $match1Finished->tournamentId());
    }

    public function testInvalidInstantiation()
    {
        $this->expectException(InvalidMatchArgumentException::class);
        $teamFactory = new TeamFactory();
        $team1 = $teamFactory->create(
            new TeamId(1), 'Team #1', 'Pele', 'Edwin van der Sar'
        );
        $teamFactory = new TeamFactory();
        $team2 = $teamFactory->create(
            new TeamId(1), 'Team #2', 'Alessandro Del Piero', 'Gianluigi Buffon'
        );
        new Match(new MatchId(1), new TournamentId(1), new TeamId($team1->id()), new TeamId($team2->id()));
    }
}
