<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Infrastructure\Persistence\InMemory;

use CODEfactors\Foosball\Domain\Shared\AggregateRoot;

abstract class InMemoryRepository
{
    protected $records;

    /*
     * Persistence of Tournament Aggregate Root to Repository
     * This method helps for further implementation of a real repository to list all places where persistence is needed
     * Remember to return always the clone version to break the reference of objects, and isolate repository data
     */
    protected function persist(AggregateRoot $record): AggregateRoot {
        if (!empty($this->records)) {
            /**
             * @var AggregateRoot $aRecord
             */
            foreach ($this->records as $key => $aRecord) {
                if ($record->id() === $aRecord->id()) {
                    // Update record
                    $this->records[$key] = $this->deepClone($record);
                    return $this->deepClone($record);
                }
            }
        }

        // Insert record
        $this->records[] = $record;
        return $this->deepClone($record);
    }

    protected function deepClone($obj)
    {
        return unserialize(serialize($obj));
    }
}
