<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Infrastructure\Persistence\Doctrine\Repository;

use CODEfactors\Foosball\Domain\Match\Model\Match;
use CODEfactors\Foosball\Domain\Match\Model\MatchFinished;
use CODEfactors\Foosball\Domain\Shared\IdentityGenerator;
use CODEfactors\Foosball\Domain\Team\Model\Team;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentFinished;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInit;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInProgress;
use CODEfactors\Foosball\Domain\Tournament\Repository\TournamentRepositoryInterface;
use CODEfactors\Foosball\Domain\Tournament\ValueObject\TournamentId;
use CODEfactors\Foosball\Infrastructure\Exception\TournamentNotFoundException;
use Doctrine\ORM\EntityRepository;

// TODO: Implement all methods from in-memory repository, and remove this inheritance at the end
class TournamentRepository extends EntityRepository implements TournamentRepositoryInterface
{
    public function create(): TournamentInit
    {
        $id = IdentityGenerator::get();
        $tournament = new TournamentInit(new TournamentId($id));
        $this->getEntityManager()->persist($tournament);
        $this->getEntityManager()->flush();
        return $tournament;
    }

    public function start(TournamentInit $tournamentInit): TournamentInProgress
    {
        $tournamentInProgress = $tournamentInit->start();
        $this->persist($tournamentInProgress);
        return $this->deepClone($tournamentInProgress);
    }

    public function finish(TournamentInProgress $tournamentInProgress): TournamentFinished
    {
        $tournamentFinished = $tournamentInProgress->finish();
        $this->persist($tournamentFinished);
        return $this->deepClone($tournamentFinished);
    }

    public function addTeam(TournamentInit $tournament, Team $team): Team
    {
        $tournament->addTeam($team);
        $this->persist($tournament);
        return $this->deepClone($team);
    }

    public function getTournamentInInitStateById(TournamentId $tournamentId): TournamentInit
    {
        $tournament = $this->getEntityManager()->find(TournamentInit::class, $tournamentId->getValue());
        if (!$tournament instanceof TournamentInit) {
            $this->throwTournamentNotFoundException($tournamentId, 'init');
        }
        return $tournament;
    }

    public function getTournamentInProgressStateById(TournamentId $tournamentId): TournamentInProgress
    {
        $tournament = $this->getEntityManager()->find(TournamentInProgress::class, $tournamentId->getValue());
        if (!$tournament instanceof TournamentInProgress) {
            $this->throwTournamentNotFoundException($tournamentId, 'progress');
        }
        return $tournament;    }

    public function getTournamentInFinishedStateById(TournamentId $tournamentId): TournamentFinished
    {
        $tournament = $this->getEntityManager()->find(TournamentFinished::class, $tournamentId->getValue());
        if (!$tournament instanceof TournamentFinished) {
            $this->throwTournamentNotFoundException($tournamentId, 'finished');
        }
        return $tournament;
    }

    public function findTournamentsInInitState(): array
    {
        if (empty($this->records)) {
            return [];
        }
        $tournamentsList = [];
        foreach ($this->records as $tournament) {
            if ($tournament instanceof TournamentInit) {
                $tournamentsList[] = $this->deepClone($tournament);
            }
        }
        return $tournamentsList;
    }

    public function findTournamentsInProgressState(): array
    {
        if (empty($this->records)) {
            return [];
        }
        $tournamentsList = [];
        foreach ($this->records as $tournament) {
            if ($tournament instanceof TournamentInProgress) {
                $tournamentsList[] = $this->deepClone($tournament);
            }
        }
        return $tournamentsList;
    }

    public function findTournamentsInFinishedState(): array
    {
        if (empty($this->records)) {
            return [];
        }
        $tournamentsList = [];
        foreach ($this->records as $tournament) {
            if ($tournament instanceof TournamentFinished) {
                $tournamentsList[] = $this->deepClone($tournament);
            }
        }
        return $tournamentsList;
    }

    public function finishMatch(
        TournamentInProgress $tournament,
        Match $match,
        int $firstTeamScore,
        int $secondTeamScore): MatchFinished
    {
        $matchFinished = $tournament->setMatchResult($match, $firstTeamScore, $secondTeamScore);
        $this->persist($tournament);
        return $this->deepClone($matchFinished);
    }

    private function throwTournamentNotFoundException(TournamentId $tournamentId, string $state)
    {
        throw new TournamentNotFoundException(
            'Tournament in state: ' . $state . ' with id: ' . $tournamentId->getValue() . ' not found'
        );
    }
}
