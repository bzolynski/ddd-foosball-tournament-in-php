<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Player\Model;

use CODEfactors\Foosball\Domain\Player\Exception\InvalidStrikerNameException;

class Striker
{
    private $name;

    public function __construct(string $name)
    {
        if (empty(trim($name))) {
            throw new InvalidStrikerNameException('Striker name cannot be empty');
        }
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }
}
