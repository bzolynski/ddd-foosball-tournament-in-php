<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\UserInterface\Response;

class BadRequestResponse
{
    public $errorMessage;

    public $code;

    public function __construct(string $errorMessage, int $code = null)
    {
        $this->errorMessage = $errorMessage;
        if ($code) {
            $this->code = $code;
        } else {
            unset($this->code);
        }
    }
}
