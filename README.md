
Domain-driven design - Foosball Tournament app in PHP
=====================================================

Journey through the DDD with Doctrine 2 ORM and MySQL, and all of that in PHP 7.1 in Slim v3 framework.
Additionally API controller uses [Slim Bridge](https://github.com/PHP-DI/Slim-Bridge).


Overview
--------

1. Tournament, and Team are Aggregate Roots.
2. Match is an entity which exists only in the context of Tournament Root Aggregate, as the collection of matches.
3. Use of explicit state modelling for Tournament instead of State Pattern. It's a better choice from the domain point
of view, and harder but still achievable to implement from the ORM persistence point of view.
4. In-memory repositories separation from domain by object deep cloning. You can find more about the advantages and
motivation in https://bitbucket.org/bzolynski/codefactors/src in DomainDrivenDesign/InMemoryRepositoriesSeparation
example.


Installation
------------

1. Build containers for Docker by executing `docker/build.sh` script.
2. Then, `make start` to start containers. `make stop` stops all containers.


Usage
-----

1. API endpoints access: `http://localhost/<endpoint>`.
2. Database access: `0.0.0.0:3306`, user: `user`, password: `password`, database: `main`.
