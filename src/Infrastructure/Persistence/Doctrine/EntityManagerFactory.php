<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Infrastructure\Persistence\Doctrine;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\YamlDriver;

class EntityManagerFactory
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param string $dbHost
     * @param string $dbUser
     * @param string $dbPass
     * @param string $dbName
     */
    public function __construct($dbHost, $dbUser, $dbPass, $dbName)
    {
        $paths = array("../src/Domain/");
        $isDevMode = false;

        $dbParams = array(
            'driver' => 'pdo_mysql',
            'host' => $dbHost,
            'user' => $dbUser,
            'password' => $dbPass,
            'dbname' => $dbName
        );

        $config = Setup::createAnnotationMetadataConfiguration(
            $paths,
            $isDevMode,
            null,
            new ArrayCache(),
            false
        );

        $this->setEntitiesYmlMapping($config);

        // TODO: On production, use "true" once when deploying a new code, then keep it "false"
        $config->setAutoGenerateProxyClasses(true);

        $this->entityManager = EntityManager::create($dbParams, $config);
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param Configuration $config
     */
    private function setEntitiesYmlMapping(Configuration $config)
    {
        $driver = new YamlDriver(array('../src/Infrastructure/Persistence/Doctrine/Mapping/'));
        $config->setMetadataDriverImpl($driver);
    }
}
