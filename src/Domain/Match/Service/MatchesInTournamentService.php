<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Match\Service;

use CODEfactors\Foosball\Domain\Match\Model\Match;
use CODEfactors\Foosball\Domain\Match\ValueObject\MatchId;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;
use CODEfactors\Foosball\Domain\Tournament\Model\MatchesCollection;
use CODEfactors\Foosball\Domain\Tournament\Model\TeamsIdCollection;
use CODEfactors\Foosball\Domain\Tournament\ValueObject\TournamentId;

class MatchesInTournamentService
{
    private $teamsIdCollection;

    private $tournamentId;

    /**
     * @var MatchesCollection
     */
    private $matchesCollection;

    public function __construct(TeamsIdCollection $teamsIdCollection, TournamentId $tournamentId)
    {
        $this->teamsIdCollection = $teamsIdCollection;
        $this->tournamentId = $tournamentId;
        $this->createMatches();
    }

    public function getMatchesCollection(): MatchesCollection
    {
        return $this->matchesCollection;
    }

    private function createMatches(): void
    {
        $matchId = 0;
        $this->matchesCollection = new MatchesCollection();
        /** @var TeamId $firstTeamId */
        /** @var TeamId $secondTeamId */
        foreach ($this->teamsIdCollection->getAll() as $firstTeamId) {
            foreach ($this->teamsIdCollection->getAll() as $secondTeamId) {
                if (!$firstTeamId->equals($secondTeamId)) {
                    $match = new Match(
                        new MatchId($matchId + 1),
                        $this->tournamentId,
                        $firstTeamId,
                        $secondTeamId
                    );
                    if (!$this->isMatchInPool($match)) {
                        $this->matchesCollection->add($match);
                        $matchId += 1;
                    }
                }
            }
        }
    }

    private function isMatchInPool(Match $match): bool
    {
        if ($this->matchesCollection->isEmpty()) {
            return false;
        }
        foreach ($this->matchesCollection->getAll() as $aMatch) {
            if ($match->equals($aMatch)) {
                return true;
            }
        }
        return false;
    }
}
