<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Team\Exception;

use Exception;

class InvalidTeamNameException extends Exception
{

}
