<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Team\Repository;

use CODEfactors\Foosball\Domain\Team\Model\Team;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;

interface TeamRepositoryInterface
{
    public function create(string $name, string $strikerName, string $goalKeeperName): Team;

    public function getById(TeamId $teamId): Team;
}
