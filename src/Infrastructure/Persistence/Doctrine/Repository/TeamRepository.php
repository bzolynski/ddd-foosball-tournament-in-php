<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Infrastructure\Persistence\Doctrine\Repository;

use CODEfactors\Foosball\Domain\Team\Repository\TeamRepositoryInterface;
use CODEfactors\Foosball\Infrastructure\Persistence\InMemory\InMemoryTeamRepository;

class TeamRepository extends InMemoryTeamRepository implements TeamRepositoryInterface
{
    // TODO: Implement all methods from in-memory repository, and remove this inheritance at the end
}
