<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Application;

use CODEfactors\Foosball\Domain\Match\Exception\MatchNotFoundException;
use CODEfactors\Foosball\Domain\Match\Model\Match;
use CODEfactors\Foosball\Domain\Match\Model\MatchFinished;
use CODEfactors\Foosball\Domain\Match\Model\MatchInterface;
use CODEfactors\Foosball\Domain\Team\Model\Team;
use CODEfactors\Foosball\Domain\Team\Repository\TeamRepositoryInterface;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;
use CODEfactors\Foosball\Domain\Tournament\Model\MatchesCollection;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentFinished;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInit;
use CODEfactors\Foosball\Domain\Tournament\Model\TournamentInProgress;
use CODEfactors\Foosball\Domain\Tournament\Repository\TournamentRepositoryInterface;
use CODEfactors\Foosball\Domain\Tournament\ValueObject\TournamentId;

class TournamentService
{
    private $tournamentRepository;

    private $teamRepository;

    public function __construct(
        TournamentRepositoryInterface $tournamentRepository,
        TeamRepositoryInterface $teamRepository
    ) {
        $this->tournamentRepository = $tournamentRepository;
        $this->teamRepository = $teamRepository;
    }

    public function createTournament(): TournamentInit
    {
        return $this->tournamentRepository->create();
    }

    public function startTournament(int $tournamentId): TournamentInProgress
    {
        $tournamentId = new TournamentId($tournamentId);
        $tournamentInit = $this->tournamentRepository->getTournamentInInitStateById($tournamentId);
        $tournamentInProgress = $this->tournamentRepository->start($tournamentInit);
        return $tournamentInProgress;
    }

    public function getTournamentInInitState(int $tournamentId): TournamentInit
    {
        $tournamentId = new TournamentId($tournamentId);
        return $this->tournamentRepository->getTournamentInInitStateById($tournamentId);
    }

    public function getTournamentInProgressState(int $tournamentId): TournamentInProgress
    {
        $tournamentId = new TournamentId($tournamentId);
        return $this->tournamentRepository->getTournamentInProgressStateById($tournamentId);
    }

    public function getTournamentInFinishedState(int $tournamentId): TournamentFinished
    {
        $tournamentId = new TournamentId($tournamentId);
        return $this->tournamentRepository->getTournamentInFinishedStateById($tournamentId);
    }

    public function findTournamentsInInitState(): array
    {
        return $this->tournamentRepository->findTournamentsInInitState();
    }

    public function findTournamentsInProgressState(): array
    {
        return $this->tournamentRepository->findTournamentsInProgressState();
    }

    public function findTournamentsInFinishedState(): array
    {
        return $this->tournamentRepository->findTournamentsInFinishedState();
    }

    public function addTeamToTournament(
        int $tournamentId,
        int $teamId
    ): Team {
        $team = $this->teamRepository->getById(new TeamId($teamId));
        $tournamentId = new TournamentId($tournamentId);

        $tournament = $this->tournamentRepository->getTournamentInInitStateById($tournamentId);
        return $this->tournamentRepository->addTeam($tournament, $team);
    }

    public function createTeam(string $name, string $strikerName, string $goalKeeperName): Team
    {
        return $this->teamRepository->create($name, $strikerName, $goalKeeperName);
    }

    public function getMatchesForTournamentInProgress(int $tournamentId): MatchesCollection
    {
        $tournament = $this->tournamentRepository->getTournamentInProgressStateById(new TournamentId($tournamentId));
        return $tournament->matches();
    }

    public function finishMatch(
        int $tournamentId,
        int $matchId,
        int $firstTeamScore,
        int $secondTeamScore): MatchFinished
    {
        $tournament = $this->tournamentRepository->getTournamentInProgressStateById(new TournamentId($tournamentId));
        $matchesCollection = $tournament->matches();
        if (!$matchesCollection->isEmpty()) {
            foreach ($matchesCollection->getAll() as $match) {
                /** @var MatchInterface $match */
                if ($match->id() === $matchId && $match instanceof Match) {
                    return $this->tournamentRepository->finishMatch(
                        $tournament,
                        $match,
                        $firstTeamScore,
                        $secondTeamScore
                    );
                }
            }
        }
        throw new MatchNotFoundException('Match with id: ' . $matchId . ' not found, or not finished');
    }

    public function finishTournament(int $tournamentId): TournamentFinished {
        $tournamentInProgress = $this->tournamentRepository->getTournamentInProgressStateById(
            new TournamentId($tournamentId)
        );
        return $this->tournamentRepository->finish($tournamentInProgress);
    }

    public function getWinnerTeamForTournament(int $tournamentId): int {
        $tournament = $this->tournamentRepository->getTournamentInFinishedStateById(new TournamentId($tournamentId));
        return $tournament->getWinnerTeamId();
    }
}
