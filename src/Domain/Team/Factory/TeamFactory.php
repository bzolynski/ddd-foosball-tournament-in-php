<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Team\Factory;

use CODEfactors\Foosball\Domain\Player\Model\GoalKeeper;
use CODEfactors\Foosball\Domain\Player\Model\Striker;
use CODEfactors\Foosball\Domain\Team\Model\Team;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;

class TeamFactory
{
    public function create(TeamId $teamId, string $name, string $strikerName, string $goalKeeperName): Team
    {
        $striker = new Striker($strikerName);
        $goalKeeper = new GoalKeeper($goalKeeperName);
        return new Team($teamId, $name, $striker, $goalKeeper);
    }
}
