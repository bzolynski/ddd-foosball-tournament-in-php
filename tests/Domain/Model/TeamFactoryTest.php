<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Tests\Domain\Model;

use CODEfactors\Foosball\Domain\Team\Exception\InvalidTeamNameException;
use CODEfactors\Foosball\Domain\Player\Model\GoalKeeper;
use CODEfactors\Foosball\Domain\Player\Model\Striker;
use CODEfactors\Foosball\Domain\Team\Factory\TeamFactory;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;
use PHPUnit\Framework\TestCase;

class TeamFactoryTest extends TestCase
{
    public function testInstantiation()
    {
        $teamFactory = new TeamFactory();
        $team = $teamFactory->create(new TeamId(1), 'Team #1', 'Pele', 'Edwin van der Sar');
        $this->assertInstanceOf(Striker::class, $team->striker());
        $this->assertInstanceOf(GoalKeeper::class, $team->goalKeeper());
        $this->assertSame('Team #1', $team->name());
        $this->assertSame('Pele', $team->striker()->name());
        $this->assertSame('Edwin van der Sar', $team->goalKeeper()->name());
    }

    public function testInvalidNameTeamInstantiation()
    {
        $this->expectException(InvalidTeamNameException::class);
        $teamFactory = new TeamFactory();
        $teamFactory->create(new TeamId(1), ' ', 'Pele', 'Edwin van der Sar');
    }
}
