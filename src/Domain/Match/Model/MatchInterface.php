<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Match\Model;

use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;

interface MatchInterface
{
    public function id(): int;

    public function tournamentId(): int;

    public function firstTeamId(): TeamId;

    public function secondTeamId(): TeamId;
}
