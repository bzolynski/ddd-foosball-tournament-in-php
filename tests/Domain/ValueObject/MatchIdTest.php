<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Tests\Domain\ValueObject;

use CODEfactors\Foosball\Domain\Match\ValueObject\MatchId;
use PHPUnit\Framework\TestCase;

class MatchIdTest extends TestCase
{
    public function testEquals()
    {
        $matchId = new MatchId(1);
        $this->assertTrue($matchId->equals(new MatchId(1)));
        $this->assertSame(1, $matchId->getValue());
    }
}
