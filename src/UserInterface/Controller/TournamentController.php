<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\UserInterface\Controller;

use CODEfactors\Foosball\Application\TournamentService;
use CODEfactors\Foosball\Infrastructure\Exception\TournamentNotFoundException;
use CODEfactors\Foosball\Infrastructure\Persistence\Doctrine\EntityManagerFactory;
use CODEfactors\Foosball\Infrastructure\Persistence\Doctrine\Repository\TeamRepository;
use CODEfactors\Foosball\Infrastructure\Persistence\Doctrine\Repository\TournamentRepository;
use CODEfactors\Foosball\UserInterface\Response\BadRequestResponse;
use CODEfactors\Foosball\UserInterface\Response\TournamentResponse;
use Doctrine\ORM\EntityManager;
use Slim\Http\Request;
use Slim\Http\Response;

class TournamentController
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TournamentService
     */
    private $tournamentService;

    public function __construct(EntityManagerFactory $entityManagerService)
    {
        $this->entityManager = $entityManagerService->getEntityManager();
    }

    public function getTournamentInInitStateById($id, Request $request, Response $response)
    {
        $tournamentService = $this->getTournamentService();
        try {
            $tournament = $tournamentService->getTournamentInInitState((int)$id);
            $tournamentResponse = new TournamentResponse($tournament);
            $newResponse = $response->withJson($tournamentResponse, 200);
        } catch (TournamentNotFoundException $e) {
            $newResponse = $response->withJson(
                new BadRequestResponse('Tournament does not exist'), 400
            );
        }
        return $newResponse;
    }

    public function createTournament(Request $request, Response $response)
    {
        $tournamentService = $this->getTournamentService();
        $tournament = $tournamentService->createTournament();
        $tournamentResponse = new TournamentResponse($tournament);
        $newResponse = $response->withJson($tournamentResponse, 200);
        return $newResponse;
    }

    /**
     * @return TournamentService
     */
    private function getTournamentService()
    {
        if ($this->tournamentService instanceof TournamentService) {
            return $this->tournamentService;
        }
        /** @var TournamentRepository $tournamentRepository */
        $tournamentRepository = $this->entityManager->getRepository(
            'CODEfactors\Foosball\Domain\Tournament\Model\Tournament'
        );
        /** @var TeamRepository $teamRepository */
        $teamRepository = $this->entityManager->getRepository(
            'CODEfactors\Foosball\Domain\Team\Model\Team'
        );
        $this->tournamentService = new TournamentService(
            $tournamentRepository, $teamRepository
        );

        return $this->tournamentService;
    }
}