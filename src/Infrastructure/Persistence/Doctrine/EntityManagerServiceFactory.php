<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Infrastructure\Persistence\Doctrine;

class EntityManagerServiceFactory
{
    const LIVE_ENVIRONMENT = 'live';

    const TEST_ENVIRONMENT = 'test';

    const DEV_ENVIRONMENT = 'dev';

    public static function create($environmentName = self::TEST_ENVIRONMENT): EntityManagerFactory
    {
        $configuration = require(self::getConfigFilename($environmentName));
        $dbHost = $configuration['db_host'];
        $dbUser = $configuration['db_user'];
        $dbPass = $configuration['db_pass'];
        $dbName = $configuration['db_name'];

        return new EntityManagerFactory($dbHost, $dbUser, $dbPass, $dbName);
    }

    public static function isValidEnvironment($environmentName): bool
    {
        return in_array($environmentName, array(
            self::DEV_ENVIRONMENT,
            self::TEST_ENVIRONMENT,
            self::LIVE_ENVIRONMENT
        ));
    }

    private static function getConfigFilename(string $environment): string
    {
        if ($environment === self::LIVE_ENVIRONMENT) {
            return '../config/db.php';
        } else if ($environment === self::DEV_ENVIRONMENT) {
            return '../config/db_dev.php';
        } else {
            return '../config/db_test.php';
        }
    }
}
