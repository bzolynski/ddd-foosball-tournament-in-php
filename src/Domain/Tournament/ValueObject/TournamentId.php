<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\ValueObject;

use CODEfactors\Foosball\Domain\Shared\AggregateRootId;

class TournamentId extends AggregateRootId
{

}
