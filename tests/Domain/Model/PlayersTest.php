<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Tests\Domain\Model;

use CODEfactors\Foosball\Domain\Player\Exception\InvalidGoalKeeperNameException;
use CODEfactors\Foosball\Domain\Player\Exception\InvalidStrikerNameException;
use CODEfactors\Foosball\Domain\Player\Model\GoalKeeper;
use CODEfactors\Foosball\Domain\Player\Model\Striker;
use PHPUnit\Framework\TestCase;

class PlayersTest extends TestCase
{
    public function testStrikerInstantiation()
    {
        $striker = new Striker('Pele');
        $this->assertSame('Pele', $striker->name());
    }

    public function testGoalKeeperInstantiation()
    {
        $goalKeeper = new GoalKeeper('Edwin van der Sar');
        $this->assertSame('Edwin van der Sar', $goalKeeper->name());
    }

    public function testInvalidStrikerNameInstantiation()
    {
        $this->expectException(InvalidStrikerNameException::class);
        new Striker(' ');
    }

    public function testInvalidGoalKeeperNameInstantiation()
    {
        $this->expectException(InvalidGoalKeeperNameException::class);
        new GoalKeeper('');
    }
}
