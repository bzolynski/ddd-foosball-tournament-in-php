<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\Model;

use CODEfactors\Foosball\Domain\Match\Model\MatchFinished;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;

class TournamentFinished extends Tournament
{
    public function __construct(TournamentInProgress $tournament)
    {
        $this->id = $tournament->id();
        $this->teams = $tournament->teams();
        $this->matches = $tournament->matches();
    }

    public function getWinnerTeamId(): int
    {
        $teamPoints = [];
        $teamGoalsFor = [];
        $teamGoalsAgainst = [];
        /** @var TeamId $teamId */
        foreach ($this->teams->getAll() as $teamId) {
            $teamPoints[$teamId->getValue()] = 0;
            $teamGoalsFor[$teamId->getValue()] = 0;
            $teamGoalsAgainst[$teamId->getValue()] = 0;
        }
        /** @var MatchFinished $match */
        foreach ($this->matches->getAll() as $match) {
            if ($match->firstTeamScore() > $match->secondTeamScore()) {
                $teamPoints[$match->firstTeamId()->getValue()] += 3;
            } else if ($match->firstTeamScore() < $match->secondTeamScore()) {
                $teamPoints[$match->secondTeamId()->getValue()] += 3;
            } else {
                $teamPoints[$match->firstTeamId()->getValue()] += 1;
                $teamPoints[$match->secondTeamId()->getValue()] += 1;
            }
            $teamGoalsFor[$match->firstTeamId()->getValue()] += $match->firstTeamScore();
            $teamGoalsFor[$match->secondTeamId()->getValue()] += $match->secondTeamScore();
            $teamGoalsAgainst[$match->firstTeamId()->getValue()] += $match->secondTeamScore();
            $teamGoalsAgainst[$match->secondTeamId()->getValue()] += $match->firstTeamScore();
        }

        $bestTeamId = 0;
        $highestScore = 0;
        foreach ($teamPoints as $teamId => $teamScore) {
            if ($teamScore > $highestScore) {
                $highestScore = $teamScore;
                $bestTeamId = $teamId;
            } else if ($bestTeamId > 0 && $teamScore === $highestScore) {
                if ($teamGoalsFor[$teamId] > $teamGoalsFor[$bestTeamId]) {
                    $bestTeamId = $teamId;
                } else if ($teamGoalsFor[$teamId] === $teamGoalsFor[$bestTeamId]) {
                    if ($teamGoalsAgainst[$teamId] < $teamGoalsAgainst[$bestTeamId]) {
                        $bestTeamId = $teamId;
                    }
                }
            }
        }
        return $bestTeamId;
    }
}
