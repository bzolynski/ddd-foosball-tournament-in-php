<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\Exception;

use Exception;

class TournamentFinishException extends Exception
{

}
