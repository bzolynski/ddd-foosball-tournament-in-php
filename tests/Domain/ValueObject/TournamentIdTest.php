<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Tests\Domain\ValueObject;

use CODEfactors\Foosball\Domain\Tournament\ValueObject\TournamentId;
use PHPUnit\Framework\TestCase;

class TournamentIdTest extends TestCase
{
    public function testEquals()
    {
        $tournamentId = new TournamentId(1);
        $this->assertTrue($tournamentId->equals(new TournamentId(1)));
        $this->assertSame(1, $tournamentId->getValue());
    }
}
