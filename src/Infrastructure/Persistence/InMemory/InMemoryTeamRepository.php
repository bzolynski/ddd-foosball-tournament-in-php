<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Infrastructure\Persistence\InMemory;

use CODEfactors\Foosball\Domain\Shared\IdentityGenerator;
use CODEfactors\Foosball\Domain\Team\Exception\TeamNotFoundException;
use CODEfactors\Foosball\Domain\Team\Factory\TeamFactory;
use CODEfactors\Foosball\Domain\Team\Model\Team;
use CODEfactors\Foosball\Domain\Team\Repository\TeamRepositoryInterface;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;

class InMemoryTeamRepository extends InMemoryRepository implements TeamRepositoryInterface
{
    public function create(string $name, string $strikerName, string $goalKeeperName): Team
    {
        $id = IdentityGenerator::get();
        $teamFactory = new TeamFactory();
        $team = $teamFactory->create(new TeamId($id), $name, $strikerName, $goalKeeperName);
        return $this->persist($team);
    }

    public function getById(TeamId $teamId): Team
    {
        /**
         * @var Team $aTeam
         */
        if (!empty($this->records)) {
            foreach ($this->records as $aTeam) {
                if ($teamId->getValue() === $aTeam->id()) {
                    return $aTeam;
                }
            }
        }
        return $this->throwTeamNotFoundException($teamId);
    }

    private function throwTeamNotFoundException(TeamId $teamId)
    {
        throw new TeamNotFoundException('Team with id: ' . $teamId->getValue() . ' not found');
    }
}
