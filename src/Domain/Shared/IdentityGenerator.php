<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Shared;

class IdentityGenerator
{
    public static function get(): int
    {
        return (int)(rand(100, 999) . date('His') );
    }
}
