<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Match\Model;

use CODEfactors\Foosball\Domain\Shared\Entity;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;

class MatchFinished extends Entity implements MatchInterface
{
    private $firstTeamId;

    private $secondTeamId;

    private $tournamentId;

    private $firstTeamScore;

    private $secondTeamScore;

    public function __construct(Match $match, int $firstTeamScore, int $secondTeamScore) {
        $this->id = $match->id();
        $this->firstTeamId = $match->firstTeamId();
        $this->secondTeamId = $match->secondTeamId();
        $this->tournamentId = $match->tournamentId();
        $this->firstTeamScore = $firstTeamScore;
        $this->secondTeamScore = $secondTeamScore;
    }

    public function tournamentId(): int
    {
        return $this->tournamentId;
    }

    public function firstTeamId(): TeamId
    {
        return $this->firstTeamId;
    }

    public function secondTeamId(): TeamId
    {
        return $this->secondTeamId;
    }

    public function firstTeamScore(): int
    {
        return $this->firstTeamScore;
    }

    public function secondTeamScore(): int
    {
        return $this->secondTeamScore;
    }
}
