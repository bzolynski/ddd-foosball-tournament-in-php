<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\Model;

use CODEfactors\Foosball\Domain\Match\Model\Match;
use CODEfactors\Foosball\Domain\Match\Model\MatchFinished;
use CODEfactors\Foosball\Domain\Tournament\Exception\TournamentFinishException;
use CODEfactors\Foosball\Domain\Tournament\Policy\FinishTournamentPolicy;

class TournamentInProgress extends Tournament
{
    public function __construct(TournamentInit $tournament, MatchesCollection $matchesCollection)
    {
        $this->id = $tournament->id();
        $this->teams = $tournament->teams();
        $this->matches = $matchesCollection;
    }

    public function setMatchResult(Match $match, int $firstTeamScore, int $secondTeamScore): MatchFinished
    {
        $matchFinished = $match->finish($firstTeamScore, $secondTeamScore);
        $this->matches->setResult($matchFinished);
        return $matchFinished;
    }

    public function finish(): TournamentFinished
    {
        if (!FinishTournamentPolicy::isAllowed($this->matches)) {
            throw new TournamentFinishException(
                "Some matches weren't played yet, unable to finish the tournament"
            );
        }
        return new TournamentFinished($this);
    }
}
