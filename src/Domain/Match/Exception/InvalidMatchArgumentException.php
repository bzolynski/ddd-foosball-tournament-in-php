<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Match\Exception;

use Exception;

class InvalidMatchArgumentException extends Exception
{

}
