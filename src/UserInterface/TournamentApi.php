<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\UserInterface;

use CODEfactors\Foosball\Infrastructure\Persistence\Doctrine\EntityManagerServiceFactory;
use DI\Bridge\Slim\App;
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;

class TournamentApi extends App
{
    protected function configureContainer(ContainerBuilder $builder)
    {
        $definitions = array(
            'CODEfactors\Foosball\Infrastructure\Persistence\Doctrine\EntityManagerFactory' => function (ContainerInterface $c) {
                $entityManagerServiceFactory = new EntityManagerServiceFactory();
                return $entityManagerServiceFactory->create($this->getEnvironment());
            },
        );
        $builder->addDefinitions($definitions);
        $builder->addDefinitions(__DIR__ . '/../../config/app.php');
    }

    private function getEnvironment(): string
    {
        $environmentName = getenv('ENVIRONMENT');
        if ($environmentName) {
            if (EntityManagerServiceFactory::isValidEnvironment($environmentName)) {
                return $environmentName;
            }
        }
        return EntityManagerServiceFactory::LIVE_ENVIRONMENT;
    }
}
