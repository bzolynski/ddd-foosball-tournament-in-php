<?php

declare(strict_types=1);

namespace CODEfactors\Foosball\Domain\Tournament\Model;

use CODEfactors\Foosball\Domain\Match\Service\MatchesInTournamentService;
use CODEfactors\Foosball\Domain\Team\Exception\DuplicatedTeamException;
use CODEfactors\Foosball\Domain\Team\Model\Team;
use CODEfactors\Foosball\Domain\Team\ValueObject\TeamId;
use CODEfactors\Foosball\Domain\Tournament\Exception\TournamentStartException;
use CODEfactors\Foosball\Domain\Tournament\Policy\StartTournamentPolicy;
use CODEfactors\Foosball\Domain\Tournament\ValueObject\TournamentId;

class TournamentInit extends Tournament
{
    public function __construct(TournamentId $tournamentId)
    {
        $this->teams = new TeamsIdCollection();
        $this->id = $tournamentId->getValue();
    }

    public function start(): TournamentInProgress
    {
        $matchesInTournamentService = new MatchesInTournamentService($this->teams, new TournamentId($this->id()));
        $matchesCollection = $matchesInTournamentService->getMatchesCollection();

        if (!StartTournamentPolicy::isAllowed($this)) {
            throw new TournamentStartException("Insufficient amount of teams");
        }
        return new TournamentInProgress($this, $matchesCollection);
    }

    public function addTeam(Team $team)
    {
        $this->validateTeam($team);
        $this->teams->add($team);
    }

    private function validateTeam(Team $team): void
    {
        if ($this->teams->isEmpty()) {
            return;
        }
        /** @var TeamId $teamId */
        foreach ($this->teams->getAll() as $teamId) {
            if ($team->id() === $teamId->getValue()) {
                throw new DuplicatedTeamException('Team already exists in tournament teams');
            }
        }
    }
}
