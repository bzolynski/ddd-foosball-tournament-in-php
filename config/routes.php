<?php

$app->get(
    '/tournaments/init/{id}',
    ['CODEfactors\Foosball\UserInterface\Controller\TournamentController', 'getTournamentInInitStateById']
);

$app->post(
    '/tournaments',
    ['CODEfactors\Foosball\UserInterface\Controller\TournamentController', 'createTournament']
);
